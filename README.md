Symfony Hotel Application
========================

In this hotel room reserevation, I have considered following things
1.There is a single room which can be booked by single username
2. Like real time application I have considered different checkin date and checkout date.
The day when a person check out,same day person can check in. Consider checkout time is 10 am.   

Requirements
------------

  * PHP 8 or higher;
  * PDO-POSTGRES PHP extension enabled;
  * and the [usual Symfony application requirements][2].

Installation
------------
https://symfony.com/download and install latest windows version
https://www.enterprisedb.com/downloads/postgres-postgresql-downloads and install 14.1 windows version
run postgressql server on port 5432
create database named hotel
Set database username: postgres
Set password: root 
php bin/console make:migration
php bin/console doctrine:migrations:migrate
symfony serve
http://localhost:8000
php ./vendor/bin/phpunit
```
see screen shot folder to screen shot of project
