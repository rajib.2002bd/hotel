<?php use App\Controller\BookingController;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

return function (RoutingConfigurator $routes) {
    $routes->add('create_booking', '/createBooking')
        ->controller([BookingController::class, 'createBooking'])
        ->methods(['GET', 'POST','PUT'])
    ;
   
	$routes->add('all_booking', '/allBooking')
        ->controller([BookingController::class, 'index'])
        ->methods(['GET'])
    ;
	$routes->add('home', '/')
        ->controller([BookingController::class, 'index'])
        ->methods(['GET'])
    ;
	$routes->add('show_booking', '/showBooking/{id}')
        ->controller([BookingController::class, 'show'])
        ->methods(['GET'])
    ;
	$routes->add('check_booking', '/checkBooking')
        ->controller([BookingController::class, 'check'])
        ->methods(['POST','GET'])
    ;
	$routes->add('date_list', '/dateList')
        ->controller([BookingController::class, 'dateList'])
        ->methods(['POST','GET'])
    ;
};
?>